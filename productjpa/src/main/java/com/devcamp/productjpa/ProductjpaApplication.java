package com.devcamp.productjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductjpaApplication.class, args);
	}

}
