package com.devcamp.productjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.productjpa.model.Product;

public interface ProductRepository  extends JpaRepository<Product, Long>{
    
}
