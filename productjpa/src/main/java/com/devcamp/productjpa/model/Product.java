package com.devcamp.productjpa.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "products")
public class Product {
@Id
    @GeneratedValue(strategy = GenerationType.AUTO) 
    private Long id;
    
    @Column(name = "ten_san_pham")
    private String tenSanPham;
    
    @Column(name = "ma_san_pham")
    private String maSanPham;
    
    @Column(name = "gia_tien")
    private Long giaTien;
    
    @Column(name = "ngay_tao")
    private Long ngayTao;
    
    @Column(name = "ngay_cap_nhat")
    private Long ngayCapNhat;
    
    public Product() {
    }

    public Product(Long id, String tenSanPham, String maSanPham, Long giaTien, Long ngayTao, Long ngayCapNhat) {
        this.id = id;
        this.tenSanPham = tenSanPham;
        this.maSanPham = maSanPham;
        this.giaTien = giaTien;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }

    public Long getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(Long giaTien) {
        this.giaTien = giaTien;
    }

    public Long getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Long getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
    
